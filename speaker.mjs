import Person from "./person.mjs";

class Speaker extends Person {
    constructor(name, age, gender)
    {
        super(name, age);

        this.gender= gender;

    }
    bio(){
        console.log(`${super.bio()} ${this.gender}`)
    }
    say(phrase) {
        console.log(`"${this.name} says ${(phrase)}" very loudly`)
    }
}

let john = new Person('John', 25);
console.log(john.bio());
console.log(john.say('Hello!'));

let bob = new Speaker('Bob', 30, 'male');
console.log(bob.bio());
console.log(bob.say('Hi!'));

