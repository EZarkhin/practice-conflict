export default class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    bio(){
        console.log(`${this.name} is ${this.age} year old`)
    }
    say(phrase) {
        return `${this.name} says ${phrase}`;
    }
}

